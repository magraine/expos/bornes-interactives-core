
[Installation]

Necessite SPIP 3.0 installé et fonctionnel. 
Ajouter le plugin dans le répertoire plugins/

Une fois le plugin actif dans SPIP, il faut activer les url particulières 
à ce jeu de squelette en editant le .htaccess à la racine de SPIP en ajoutant 
dans les parametres personnalisés :
  
RewriteRule ^credits([0-9]+)(\.html)?$	spip.php?page=credits&id_credits=$1 [QSA,L]


[Documentation des fonctionnalités]

Pour utiliser avec ce squelette :
Créer une première rubrique (= une expo).
Créer une sous-rubrique dedans (= une borne)
Créer une sous-rubrique dedans (= une planche d'une borne)
Créer un article dedans (= un des éléments de la planche)

