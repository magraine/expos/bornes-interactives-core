<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/cextras');
include_spip('base/gbi');

/**
 * Installation
**/
function gbi_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	cextras_api_upgrade(gbi_declarer_champs_extras(), $maj['create']);
	cextras_api_upgrade(gbi_declarer_champs_extras(), $maj['1.2.3']);
	cextras_api_upgrade(gbi_declarer_champs_extras(), $maj['1.2.4']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Désinstallation
**/
function gbi_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(gbi_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}
?>
