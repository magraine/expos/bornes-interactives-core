<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_titre_gbi' => 'Bornes Int&eacute;ractives',

	// L
	'label_localisation' => 'Localisation',
	'label_salle' => 'Salle',
	'label_internet' => 'Internet',

	'label_retour' => 'Retours',
	'label_accueil_exposition' => "Accueil de l'exposition",
	'label_accueil_borne' => "Accueil de la borne",
	'label_variante_borne' => "Variante de borne",
	'label_sources' => "Sources",
	'label_pos_x' => "Position X",
	'label_pos_y' => "Position Y",
	'label_data' => "Data (données brutes)",


	// L
	'explication_variante_borne' => "
		Dans certains cas, les compositions de bornes peuvent avoir une
		variante de couleur. Indiquer ici son code pour l'utiliser.",

	'explication_localisation' => 'Indiquer dans quelle condition doit se placer la borne.
			En "salle", les liens externes des crédits des documents sont désactivés,
			pour éviter que le navigateur quitte le site local. En "internet", ces liens
			sont actifs, permettant au visiteur de sortir du site.',
	'explication_retour' => "Indiquer dans quel lieu doivent retourner les boutons
			de retour des planches d'une borne.",

	// R
	'retour_au_menu' => "Retour au menu",
	'credits_photos_sons' => "Cr&eacute;dits photographiques et sonores",
);
