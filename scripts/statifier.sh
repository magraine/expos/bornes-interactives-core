#!/bin/bash

URL_BASE=http://lune/cddp/
DIR_BASE=/home/marcimat/Bureau/CDDP/Exports/
OPTIONS="--update"
#OPTIONS="--continue"
#OPTIONS="--mirror"

extraction_statique() {
	URL=$1
	DIR_EXPORT=$2

	echo
	echo
	echo "Exporter "$URL" dans "$DIR_EXPORT
	echo
	echo

	origine=$(pwd)
	mkdir -p $DIR_EXPORT
	cd $DIR_EXPORT
	httrack $URL -o0 $OPTIONS
	cd $origine
}

extraction_statique $URL_BASE"rubrique3.html" $DIR_BASE"Catalogue";
extraction_statique $URL_BASE"rubrique6.html" $DIR_BASE"Dragosphere";
extraction_statique $URL_BASE"rubrique8.html" $DIR_BASE"Quiz/Dragonologie";
extraction_statique $URL_BASE"rubrique9.html" $DIR_BASE"Quiz/Antiquite";
extraction_statique $URL_BASE"rubrique10.html" $DIR_BASE"Quiz/Azteque";


echo
echo " -- FIN -- "

